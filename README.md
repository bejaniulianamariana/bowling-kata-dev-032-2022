# bowling-kata-DEV-032-2022

Bowling Game Kata - TDD


## Name
Simple bowling game KATA - TDD approach

## Description
Given a valid sequence of rolls for one line of American Ten-Pin Bowling, the program, produces the total score for the game.

BowlingGameApplication 
	
		- main class to simulate the game. 
		- it generates randomly, for each roll of the game, numbers between 0 and 10 inclusive 
		- after all rolls the program prints the status of all rolls and prints the final score
					   
BowlingGame	
	
		- class that contains the logic to roll and calculate the score of the game
		- a bowlingGame has max 10 frames

Frame		
	
		- holds information about a frame in the game; a frame has 1,2 or 3 max rolls

Roll			
		
		- holds information about the number of pins knocked during a roll

BowlingGameTest	

		- test class to validate all possible scenarios of the game

## Prerequisites
Java 17 (JRE or JDK) installed

## Guidelines to clone package and run the application
1. Step 1: Cloning the repository using https:
	
	git clone https://gitlab.com/2022-DEV-032/bowling-kata-dev-032-2022.git
	
2. Step 2: Go to folder where the code was cloned

	cd your_project_dir/bowling-kata-dev-032-2022
	
3.a. Step 3a: Create executable using maven from command prompt

The maven package command compiles the source code, runs all of the test cases, and then creates the non-executable jar file. By including all of the required jar files, Maven will create the executable jar from the non-executable jar.
The executable and non-executable jars will be created in the target folder of the spring boot project.
	
	mvn package
	or
	mvn clean package
	
3.b. Step 3b: Create executable jar using STS(Spring-Tool-Suite)

You may create executable and non-executable jars using the Spring Tool Suite UI. The following steps will walk you through the process of creating jars in the STS UI.
	
	Select the spring boot project name
	Right Click on the project name
	Click "Run As -> Maven Build" in the popup
	A new Popup window will open
	enter "package" in the Goals input box
	Click "Run" button in the bottom right side of the popup window
	
4. Step 4: Check in target folder

The executable jar file bowling-kata-dev-032-2022-0.0.1-SNAPSHOT.jar and the non-executable jar file bowling-kata-dev-032-2022-0.0.1-SNAPSHOT.jar.original are created in the target folder in the spring boot project directory.

	your_project_dir/target/bowling-kata-dev-032-2022-0.0.1-SNAPSHOT.jar
	your_project_dir/target/bowling-kata-dev-032-2022-0.0.1-SNAPSHOT.jar.original


5. Step 5: Executing jar file

The java command can be used to execute the jar files produced by the spring boot application. To launch the executable jar, open a terminal or command prompt and type the following java command.

	your_project_dir:> java -jar target/bowling-kata-dev-032-2022-0.0.1-SNAPSHOT.jar
	
	
## Build & Testing

Build with maven
	
	mvn build
	or
	mvn clean build

Test with maven
After building the project, we can now run all the unit tests using this command below.

	
	mvn test
	or
	mvn clean test
