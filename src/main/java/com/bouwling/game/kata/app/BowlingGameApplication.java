package com.bouwling.game.kata.app;

import com.bouwling.game.kata.logic.BowlingGame;
import com.bouwling.game.kata.models.Frame;

/**
 * 
 * Main class to run the application
 * 
 * @author dev_032_2022
 *
 */
public class BowlingGameApplication {

	private static final int MAX_PINS = 10;
	private static final int MIN_PINS = 0;

	public static void main(String[] args) {
		int score = playGame();
		System.out.println("Final score of game:" + score);
	}

	public static int playGame() {
		BowlingGame game = new BowlingGame();
		for (int i = 0; i < 21; i++) {
			if (!game.isEndGame()) {
				int pins = generatePinsForRoll(game);
				game.roll(pins);
			}
		}
		game.printGame();
		return game.getScore();
	}

	public static int generatePinsForRoll(BowlingGame game) {
		Frame currentFrame = game.getCurrentFrame();
		if (currentFrame.isLast() && (currentFrame.isStrike() || currentFrame.isSpare())) {
			return MAX_PINS;
		} else {
			return remaniningPinsForCurrentFrame(game);
		}
	}

	public static int remaniningPinsForCurrentFrame(BowlingGame game) {
		Frame currentFrame = game.getCurrentFrame();
		int pinsKnockedAlready = currentFrame.getTotalPinsForFrame();
		return generateRandomNbrBetweenMinAndMax(MAX_PINS - pinsKnockedAlready);
	}

	/**
	 * generates randomly the number of pins knocked down, value can be between
	 * between 0 and 10
	 * 
	 * @return
	 */
	public static int generateRandomNbrBetweenMinAndMax(int max) {
		return (int) (Math.random() * (max - MIN_PINS + 1)) + MIN_PINS;
	}
}
