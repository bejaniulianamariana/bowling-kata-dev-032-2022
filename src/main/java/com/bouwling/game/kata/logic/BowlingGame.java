package com.bouwling.game.kata.logic;

import java.util.ArrayList;
import java.util.List;

import com.bouwling.game.kata.models.Frame;
import com.bouwling.game.kata.models.Roll;

import lombok.Getter;

/**
 * Bowling Game Class
 * 
 * <p>
 * A bowling game has 10 frames
 * </p>
 * 
 * <p>
 * We will not check for valid rolls.
 * 
 * We will not check for correct number of rolls and frames.
 * 
 * We will not provide scores for intermediate frames.
 * </p>
 * 
 * @author dev_032_2022
 *
 */
public class BowlingGame {

	private static final int MAX_FRAMES = 10;
	private static final int MAX_ROLLS_LAST_FRAME = 3;

	/**
	 * a game has 10 frames
	 */
	@Getter
	private final List<Frame> frames;

	/**
	 * the current roll in a frame - a frame can have up to 2 or 3(last frame) max
	 * rolls
	 */
	@Getter
	private int currentRollCursor;

	/**
	 * current frame of the game
	 */
	@Getter
	private int currentFrameCursor;

	public BowlingGame() {
		frames = new ArrayList<>(MAX_FRAMES);
		for (int i = 0; i < MAX_FRAMES - 1; i++) {
			frames.add(new Frame());
		}
		// adding last frame with isLast = true
		frames.add(new Frame(true));
	}

	/**
	 * Called each time a player rolls a ball
	 * 
	 * @param pins - number of pins knocked down
	 */
	public void roll(int pins) {
		if (isEndGame()) {
			return;
		}
		getFrame(currentFrameCursor).getRolls().get(currentRollCursor++).setPins(pins);
		Frame currentFrame = getCurrentFrame();
		if (currentFrame.isLast() && (currentFrame.isSpare() || currentFrame.isStrike())) {
			bonusRoll();
		}
		nextFrame();
	}

	/**
	 * Get the score of the game
	 * 
	 * @return - the total score for a game
	 */
	public int getScore() {
		int score = 0;
		for (int i = 0; i < frames.size() - 1; i++) {
			if (getFrame(i).isStrike()) {
				score += getBonusStrike(i);
			} else if (getFrame(i).isSpare()) {
				score += getBonusSpare(i);
			} else {
				score += getFrame(i).getTotalPinsForFrame();
			}
		}
		// add last frame points
		score += (getFrame(frames.size() - 1)).getTotalPinsForFrame();
		return score;
	}

	/**
	 * Moves cursor to next frame and reinitializes the roll_cursor for the frame
	 */
	public void nextFrame() {
		Frame currentFrame = getCurrentFrame();
		if (!currentFrame.isLast() && currentFrame.isStrike()
				|| currentRollCursor >= currentFrame.getRolls().size()) {
			updateCursors();
		}
	}

	public void updateCursors() {
		currentFrameCursor++;
		currentRollCursor = 0;
	}

	public Frame getCurrentFrame() {
		return frames.get(currentFrameCursor);
	}


	/**
	 * 10 points + the number of pins you knock down for your first attempt at the
	 * next frame
	 * 
	 * @param currentFame
	 * @return
	 */
	public int getBonusSpare(int currentFrame) {
		return 10 + getFrame(currentFrame + 1).getRolls().get(0).getPins();
	}

	/**
	 * 1 max Bonus roll will only be added for last frame if there was a spare
	 */
	public void bonusRoll() {
		if (getLastFrame().getRolls().size() < MAX_ROLLS_LAST_FRAME) {
			getLastFrame().addRoll(new Roll());
		}
	}

	public Frame getLastFrame() {
		return getFrame(MAX_FRAMES - 1);
	}

	public Frame getFrame(int frameCursor) {
		return frames.get(frameCursor);
	}
	

	/**
	 * 10 points + the number of pins you knock down in your next two rolls
	 * 
	 * @param currentFrame
	 * @return
	 */
	public int getBonusStrike(int currentFrame) {
		Frame nextFrame = getFrame(currentFrame + 1);
		int bonus = 10 + nextFrame.getRolls().get(0).getPins();
		if (nextFrame.isStrike() && !nextFrame.isLast()) {
			bonus += getFrame(currentFrame + 2).getRolls().get(0).getPins();
		} else {
			bonus += nextFrame.getRolls().get(1).getPins();
		}

		return bonus;
	}

	public boolean isEndGame() {
		return currentFrameCursor >= MAX_FRAMES;
	}

	/**
	 * Method to print content of all frames of the game
	 */
	public void printGame() {
		int i = 0;
		for (Frame frame : frames) {
			System.out.print("frame:" + i++ + "[");
			for (int j = 0; j < frame.getRolls().size(); j++) {
				System.out.print(frame.getRolls().get(j).getPins());
				if (j != frame.getRolls().size() - 1) {
					System.out.print(",");
				}
				if (j == frame.getRolls().size() - 1) {
					System.out.print("]");
				}
			}
			System.out.println();
		}
	}

}
