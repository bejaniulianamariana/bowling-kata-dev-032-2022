package com.bouwling.game.kata.models;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * A frame has 1 or two rolls
 * 
 * <p>
 * Only the last frame gets an additional Roll in case there is a Strike or a
 * Spare in the last frame
 * </p>
 * 
 * @author dev_032_2022
 *
 */
public class Frame {

	private static final int MAX_ROLLS_PER_FRAME = 2;
	private static final int MAX_PINS = 10;
	
	@Getter
	private final List<Roll> rolls;
	
	@Getter
	private boolean isLast;

	public Frame() {
		rolls = new ArrayList<>(MAX_ROLLS_PER_FRAME);
		for (int i = 0; i < MAX_ROLLS_PER_FRAME; i++) {
			rolls.add(new Roll());
		}
	}
	
	public Frame(boolean isLast) {
		this();
		this.isLast = true;
	}

	public void addRoll(Roll roll) {
		rolls.add(roll);
	}

	
	/**
	 * A spare is when the player knocks down all 10 pins in two tries in the frame
	 * 
	 * @param pins
	 * @return
	 */
	public boolean isSpare() {
		return getTotalPinsForFrame() == MAX_PINS;
	}
	
	/**
	 * A strike is when the player knocks down all 10 pins on his first try in the
	 * frame
	 * 
	 * @param pins
	 * @return
	 */
	public boolean isStrike() {
		return this.getRolls().get(0).getPins() == MAX_PINS;
	}
	

	/**
	 * Gets the sum of total pins in a frame
	 * 
	 * @param frame - the frame to calculate total pins from
	 * @return - the total pins for the given frame
	 */
	public int getTotalPinsForFrame() {
		int pinsPerFrame = 0;
		for (int i = 0; i < this.getRolls().size(); i++) {
			pinsPerFrame += this.getRolls().get(i).getPins();
		}
		return pinsPerFrame;
	}
}
