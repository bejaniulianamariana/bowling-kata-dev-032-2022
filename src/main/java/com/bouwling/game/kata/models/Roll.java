package com.bouwling.game.kata.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Roll class - holds the info on the number of pins knocked down during a roll
 * 
 * @author dev_032_2022
 *
 */
public class Roll {

	@Getter
	@Setter
	private int pins;
}
