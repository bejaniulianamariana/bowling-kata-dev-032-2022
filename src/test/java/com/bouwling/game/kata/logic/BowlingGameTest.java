package com.bouwling.game.kata.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BowlingGameTest {

	BowlingGame game;

	@BeforeEach
	public void setUp() {
		game = new BowlingGame();
	}

	@AfterEach
	public void cleanUp() {
		game = null;
	}

	@Test
	public void initialization() {
		assertTrue(game.getFrames().size() == 10);
		assertTrue(game.getFrames().get(0).getRolls().size() == 2);
		assertEquals(0, game.getScore());
	}

	@Test
	public void allGutter() {
		rollMultiple(0, 10);
		assertEquals(0, game.getScore());
	}

	@Test
	public void all9AndMiss() {
		rollMultiple(9, 10);
		assertEquals(90, game.getScore());
	}

	@Test
	public void all2AndMiss() {
		rollMultiple(2, 10);
		assertEquals(20, game.getScore());
	}

	@Test
	public void allOnes() {
		rollMultiple(1, 20);
		assertEquals(20, game.getScore());
	}

	@Test
	public void oneSpareRestMiss() {
		rollSpare(4);
		rollMultiple(18, 0);
		assertEquals(10, game.getScore());
		// no bonus roll added
		assertEquals(2, game.getLastFrame().getRolls().size());
	}

	@Test
	public void oneSpareFollowedBy3AndRestMiss() {
		rollSpare(4);
		game.roll(3);
		rollMultiple(17, 0);
		assertEquals(16, game.getScore());
		// no bonus roll added
		assertEquals(2, game.getLastFrame().getRolls().size());
	}

	@Test
	public void oneSpareFollowedBy3And4AndRestMiss() {
		rollSpare(4);
		game.roll(3);
		game.roll(4);
		rollMultiple(16, 0);
		assertEquals(20, game.getScore());
		// no bonus roll added
		assertEquals(2, game.getLastFrame().getRolls().size());
	}

	@Test
	public void all5Spare() {
		rollMultiple(5, 21);
		assertEquals(150, game.getScore());
		// bonus roll added
		assertEquals(3, game.getLastFrame().getRolls().size());
	}

	@Test
	public void getBonusSpare7() {
		rollSpare(8);
		game.roll(7);
		game.roll(9);
		rollMultiple(16, 0);
		assertTrue(game.getFrames().get(0).isSpare());
		assertFalse(game.getFrames().get(1).isSpare());
		assertEquals(17, game.getBonusSpare(0));
		assertEquals(33, game.getScore());
		// no bonus roll added
		assertEquals(2, game.getLastFrame().getRolls().size());
	}

	@Test
	public void oneStrikeFollowedBy3And4RestMiss() {
		rollStrike();
		game.roll(3);
		game.roll(4);
		rollMultiple(16, 0);
		assertEquals(24, game.getScore());
	}

	@Test
	public void allStrike() {
		rollMultiple(10, 12);
		assertEquals(300, game.getScore());
		// bonus roll added
		assertEquals(3, game.getLastFrame().getRolls().size());
		game.printGame();
	}

	// utility methods
	private void rollMultiple(int pins, int rolls) {
		for (int i = 0; i < rolls; i++) {
			game.roll(pins);
		}
	}

	private void rollSpare(int roll1) {
		game.roll(roll1);
		game.roll(10 - roll1);
	}

	private void rollStrike() {
		game.roll(10);
	}

}
