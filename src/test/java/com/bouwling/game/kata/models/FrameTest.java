package com.bouwling.game.kata.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.bouwling.game.kata.models.Frame;
import com.bouwling.game.kata.models.Roll;

public class FrameTest {

	Frame frame;

	@BeforeEach
	public void setUp() {
		frame = new Frame();
	}

	@AfterEach
	public void cleanUp() {
		frame = null;
	}

	@Test
	public void initialization() {
		assertNotNull(frame.getRolls());
		assertEquals(2, frame.getRolls().size());
	}
	
	@Test
	public void initialization_isLastTrue() {
		Frame isLast = new Frame(true);
		assertNotNull(isLast.getRolls());
		assertEquals(2, isLast.getRolls().size());
		assertTrue(isLast.isLast());
	}

	@Test
	public void addRoll() {
		assertNotNull(frame.getRolls());
		assertEquals(2, frame.getRolls().size());
		frame.addRoll(new Roll());
		assertEquals(3, frame.getRolls().size());

	}

	@Test
	public void isSpare() {
		Frame spare = createFrame(3, 7);
		assertTrue(spare.isSpare());
		Frame notSpare = createFrame(2, 5);
		assertFalse(notSpare.isSpare());
	}
	
	@Test
	public void isStrike() {
		Frame strike = createFrame(10, 0);
		assertTrue(strike.isStrike());
		Frame notStrike = createFrame(0, 10);
		assertFalse(notStrike.isStrike());
	}
	
	private Frame createFrame(int roll1, int roll2) {
		Frame frame = new Frame();
		frame.getRolls().get(0).setPins(roll1);
		frame.getRolls().get(1).setPins(roll2);
		return frame;
	}
}
