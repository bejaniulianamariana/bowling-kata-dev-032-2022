package com.bouwling.game.kata.app;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class BowlingGameApplicationTest {

	@Test
	public void generateRandomNbrBetween0And10() {
		int pins = BowlingGameApplication.generateRandomNbrBetweenMinAndMax(10);
		assertTrue(pins >= 0 && pins <= 10);
	}

}
